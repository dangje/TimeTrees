﻿using System;



namespace TimeTrees
{
    public static class EditProgram
    {
        public static void RunPeople()
        {
            var person = FindPeopleProgram.Run();
            var flag = true;
            do
            {
                Console.Clear();
                Console.WriteLine("1 Изменить имя \n" +
                                  "2 Изменить дату рождения \n" +
                                  "3 Изменить дату смерти \n" +
                                  "4 Добавить/Изменить родителей \n" +
                                  " \n Esc что-бы закончить");
                
                var keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1: 
                        Console.Write("Введите новое имя: ");
                        person.Name = Console.ReadLine();
                        break;
                    case ConsoleKey.D2: 
                        Console.Write("Введите новую дату рождения: ");
                        person.Death = AddProgram.AddDate();
                        break;
                    case ConsoleKey.D3: 
                        Console.Write("Введите новую дату смерти: ");
                        person.Death = AddProgram.AddDate();
                        break;
                    case ConsoleKey.D4:
                        person.Parents[0] = FindPeopleProgram.Run();
                        Console.WriteLine("F что-бы продолжить искать?");
                        var cont = Console.ReadKey(true);
                        if (cont.Key == ConsoleKey.F)
                        {
                            person.Parents[1] = FindPeopleProgram.Run();
                        }
                        break;
                    case ConsoleKey.Escape:
                        flag = false;
                        break;
                }
            } while (flag);
        }
    }
}