﻿using System;


namespace TimeTrees
{
    public static  class LeapYearProgram
    {

        public static void Run()
        {
            foreach (People man in DataProcessing.PeopleList)
                if (LeapYear(man) && Younger20(man))
                    Console.WriteLine(man.Name + " родился в високосный год");
        }
        
        static bool Younger20(People person)
        {
            var nowDate = DateTime.Now;
            if (person.Death != null)
            {
                var delDate = person.Death?.Year - person.Born.Year - 1 + (person.Death?.Month > person.Born.Month
                                                                          || person.Death?.Month == person.Born.Month
                                                                          && person.Death?.Day >= person.Born.Day ? 1 : 0);
                return (delDate < 20);
            }

            else
            {
                var delDate = nowDate.Year - person.Born.Year - 1 + ((nowDate.Month > person.Born.Month 
                                                                      || nowDate.Month == person.Born.Month 
                                                                      && nowDate.Day >= person.Born.Day) ? 1 : 0);
                return (delDate < 20);
            }
        }

        static bool LeapYear(People person)
        {
            return person.Born.Year % 400 == 0 || person.Born.Year % 4 == 0 && person.Born.Year % 100 != 0;
        }
    }
}