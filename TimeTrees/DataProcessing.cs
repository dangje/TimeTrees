﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;


namespace TimeTrees
{
     public static class DataProcessing
    {
        const byte TimelineDate = 0;
        const byte TimelineEvent = 1;
        const byte PeopleNumber = 0;
        const byte PeopleName = 1; 
        const byte PeopleBorn = 2; 
        const byte PeopleDeath = 3;

        public static List<Timeline> TimeLineList = new List<Timeline>();
        public static List<People> PeopleList = new List<People>();

        public static DateTime? ParseDate(object stDate)
        {
            var parseString = (string) stDate;
            if (parseString.Length == 4) return DateTime.ParseExact(parseString, "yyyy", null);
            else if (parseString.Length == 7) return DateTime.ParseExact(parseString, "yyyy-MM", null);
            else if (parseString.Length == 10) return DateTime.ParseExact(parseString, "yyyy-MM-dd", null);
            return null;
        }


        public static void ReadTimeLineJson()
        {
            var dateFromFile = File.ReadAllText(@"..\..\..\..\timelime.json");
            TimeLineList = !string.IsNullOrEmpty(dateFromFile)
                ? JsonConvert.DeserializeObject<Timeline[]>(dateFromFile).ToList()
                : ReadTimelineCsv();
        }


        public static void ReadPeopleJson()
        {
            var dateFromFile = File.ReadAllText(@"..\..\..\..\people.json");
            PeopleList = !string.IsNullOrEmpty(dateFromFile)
                ? JsonConvert.DeserializeObject<People[]>(dateFromFile).ToList()
                : ReadPeopleCsv();
        }


        public static void WritePeople()
        { 
            File.WriteAllText(@"..\..\..\..\people.json",JsonConvert.SerializeObject(PeopleList));
        }

        
        public static void WriteTimeLine()
        {
            File.WriteAllText(@"..\..\..\..\timelime.json", JsonConvert.SerializeObject(TimeLineList));
        }


        static List<Timeline> ReadTimelineCsv()
        {
            
            string[][] splitData = SplitData(File.ReadAllLines(@"..\..\..\..\timelime.csv"));
            List<Timeline> resList = new List<Timeline>();
            Timeline oneEvent = new Timeline();
            for (byte el = 0; el < splitData.Length; el++)
            {
                oneEvent.Date = (DateTime) ParseDate(splitData[el][TimelineDate]);
                oneEvent.Event = splitData[el][TimelineEvent];
                resList.Add(oneEvent);
            }
            
            return resList;
        }
        
        
        static List<People> ReadPeopleCsv()
        {
            string[][] splitData = SplitData(File.ReadAllLines(@"..\..\..\..\people.csv"));
            List<People> resList = new List<People>();
            People man = new People();
            for (byte el = 0; el < splitData.Length; el++)
            {
                man.Id = int.Parse(splitData[el][PeopleNumber]);
                man.Name = splitData[el][PeopleName];
                man.Born = (DateTime) ParseDate(splitData[el][PeopleBorn]);
                man.Death = (splitData[el].Length == 4)
                    ? ParseDate(splitData[el][PeopleDeath])
                    : man.Death = null;
                resList.Add(man);
            }

            return resList;
        }
        
        
        static string[][] SplitData(string[] data)
        {
            string[][] split = new string[data.Length][];
            for (short i = 0; i < data.Length; i++)
            {
                split[i] = data[i].Split(';');
            }
            return split;
        }
    }
}