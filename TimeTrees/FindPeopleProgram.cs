﻿ using System;
 using System.Collections.Generic;
 using System.Text;

 namespace TimeTrees
 {
     public class FindPeopleProgram
     {
         public static People Run()
         {
             var selectedPersonId = 0;
             StringBuilder inputName = new StringBuilder();
             do
             {
                 Console.Clear();
                 Console.WriteLine("ПОИСК");
                 Console.WriteLine($"Введите мия: {inputName}");
                 var printPeople = inputName.Length == 0
                     ? DataProcessing.PeopleList
                     : FilterPeople(inputName.ToString());
                 PrintPeopleMenu(printPeople, selectedPersonId);
                 ConsoleKeyInfo key = Console.ReadKey(true);
                 if (Char.IsLetter(key.KeyChar))
                     inputName.Append(key.KeyChar);
                 else
                 {
                     switch (key.Key)
                     {
                         case ConsoleKey.Backspace:
                             inputName.Remove(0, inputName.Length);

                             break; 
                         case ConsoleKey.DownArrow:
                             selectedPersonId = selectedPersonId < printPeople.Count - 1
                                 ? ++selectedPersonId
                                 : 0;
                              
                             break;
                         case ConsoleKey.UpArrow:
                             selectedPersonId = selectedPersonId != 0
                                 ? --selectedPersonId
                                 : printPeople.Count - 1;
                             
                             break;
                         case ConsoleKey.Enter:
                             return printPeople[selectedPersonId];
                     }
                 }
             } while (true) ;
         }

         static void PrintPeopleMenu(List<People> people, int selectedPersonNum)
         {
             
             for (var i = 0; i < people.Count; i++)
             {
                 if (selectedPersonNum == i)
                     Console.BackgroundColor = ConsoleColor.White;
                 Console.WriteLine(people[i].Name);
                 Console.BackgroundColor = ConsoleColor.Black;
             }
         }
         static List<People> FilterPeople(string inputName)
         {
             var filterList =  new List<People>();
             foreach (var person in DataProcessing.PeopleList)
             {
                 if (person.Name.StartsWith(inputName))
                 {
                     filterList.Add(person);
                 }
             }
             return filterList;
         }
     }
}