﻿using System;
using System.Collections.Generic;
using System.IO;


namespace TimeTrees
{
    public static  class DeltaProgram
    {
        public static  void Run()
        {
            
            (var year, var month, var day) = DelDate(DataProcessing.TimeLineList);
            Console.WriteLine($"Между событиями прошло {year} лет {month} месяцев {day} дней");
        }
        static (int, int, int) DelDate(List<Timeline> timeline)
        {
            var resDate = DateTime.MinValue;
            (var min, var max) =
             MinAndMaxDate(timeline);
            TimeSpan diff = max - min;
            resDate += diff;
            return (resDate.Year - 1, resDate.Month - 1, resDate.Day - 1);
        }

        static (DateTime, DateTime) MinAndMaxDate(List<Timeline> timeline)
        {
            var minDate = DateTime.MaxValue;
            var maxDate = DateTime.MinValue;
            foreach (Timeline ev in timeline)
            {
                minDate = minDate > ev.Date ? ev.Date : minDate;
                maxDate = maxDate < ev.Date ? ev.Date : maxDate;

            }

            return (minDate, maxDate);
        }
    }
}