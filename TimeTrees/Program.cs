﻿using System;
using System.Collections.Generic;


namespace TimeTrees
{
    class Program
    {
        const byte RunDeltaProgram = 1; 
        const byte RunLeapYearProgram = 2;
        const byte RunAddPeople = 3;
        const byte RunAddTimeEvent = 4;
        const byte RunEditPeople = 5;
        const byte RunDisplayProgram = 6;

        class MenuItem
        {
            public byte Id;
            public string Text;
        }
        
        static List<MenuItem> MenuList = new List<MenuItem>
        {
            new MenuItem {Id = RunDeltaProgram, Text = "Найти разницу дат между событиями"},
            new MenuItem {Id = RunLeapYearProgram, Text = "Люди родившиеся в високосный год"},
            new MenuItem {Id = RunAddPeople, Text = "Добавить человека"},
            new MenuItem {Id = RunAddTimeEvent, Text = "Добавить событие"},
            new MenuItem {Id = RunEditPeople, Text = "Редактировать человека"},
            new MenuItem {Id = RunDisplayProgram, Text = "Вывести на экран"}
            
        };

        
        static void PrintMenu(List<MenuItem> menuList)
        {
            Console.Clear();
            foreach (var item in menuList)
            {
                Console.WriteLine($"{item.Id} {item.Text}");
            }
        }

        static void Menu()
        {
            var continueProgram = true;
            do
            {
                PrintMenu(MenuList);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                Console.Clear();
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        DeltaProgram.Run();
                        break;
                    case ConsoleKey.D2:
                        LeapYearProgram.Run();
                        break;
                    case ConsoleKey.D3:
                        AddProgram.RunPeople();
                        break;
                    case ConsoleKey.D4:
                        AddProgram.RunTimeEvent();
                        break;
                    case ConsoleKey.D5:
                        EditProgram.RunPeople();
                        break;
                    case ConsoleKey.D6:
                        DisplayProgram.Run();
                        break;
                }

                Console.WriteLine("\n \n Продолжить выполнение программы? Y/N");
                ConsoleKeyInfo cont = Console.ReadKey(true);
                if (cont.Key == ConsoleKey.Y) Console.Clear();
                else if (cont.Key == ConsoleKey.N) continueProgram = false;
            } while (continueProgram);
        }
        static void Main()
        {
            DataProcessing.ReadTimeLineJson();
            DataProcessing.ReadPeopleJson();
            Menu();
            DataProcessing.WriteTimeLine();
            DataProcessing.WritePeople();
            
        }
    }
}