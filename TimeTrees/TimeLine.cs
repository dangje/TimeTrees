﻿using System;
using System.Collections.Generic;


namespace TimeTrees
{
    public class Timeline
    {
        public DateTime Date;
        public string Event;
        public List<People> Peoples = new List<People>();
    }
}