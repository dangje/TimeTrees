﻿using System;


namespace TimeTrees
{
    public static  class AddProgram
    {
        public static void RunPeople()
        {
            People person = new People();
            var rnd = new Random();
            do
            {
                person.Id = rnd.Next();
            } while (!CheckId(person.Id));
            person.Id = rnd.Next();
            Console.WriteLine("Ввод нового человека");
            Console.Write("Введите имя: ");
            person.Name = Console.ReadLine();
            Console.Write("Введите дату рождения: ");
            DateTime? dateBorn;
            do
            {
                dateBorn = AddDate();
            } while (dateBorn == null);
            person.Born = (DateTime) dateBorn;
            
            Console.Write("Введите дату смерти: ");
            var dateDeath = AddDate();
            while (dateDeath < dateBorn)
            {
                Console.Write("Введите корректную дату смерти: ");
                dateDeath = AddDate();
            }
            person.Death = dateDeath;
            Console.WriteLine("Нажмите F чтобы добавить родителей");
            ConsoleKeyInfo keyInfo = Console.ReadKey(true);
            if (keyInfo.Key == ConsoleKey.F)
            {
                person.Parents[0] = FindPeopleProgram.Run();
                Console.WriteLine("F что-бы продолжить искать?");
                keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.F)
                {
                    person.Parents[1] = FindPeopleProgram.Run();
                }
            }
            DataProcessing.PeopleList.Add(person);
        }

        public static void RunTimeEvent()
        {
            Timeline timeEvent = new Timeline();
            Console.WriteLine("Ввод нового события");
            Console.Write("Введите дату: ");
            DateTime? date;
            do
            {
                date = AddDate();
            } while (date == null);
            timeEvent.Date = (DateTime) date;
            Console.Write("Введите событие: ");
            timeEvent.Event = Console.ReadLine();
            var flag = true;
            do
            {
                Console.WriteLine("Нажмите F чтобы добавить участников");
                ConsoleKeyInfo yesNot = Console.ReadKey(true);
                if (yesNot.Key == ConsoleKey.F) timeEvent.Peoples.Add(FindPeopleProgram.Run());
                else flag = false;
            } while (flag);

            DataProcessing.TimeLineList.Add(timeEvent);
        }

        public static DateTime? AddDate()
        {
            string inputDate = Console.ReadLine();
            DateTime? date;
            if (inputDate is null) date = null;
            else
            {
                date = DataProcessing.ParseDate(inputDate);
                while (date == null)
                {
                    Console.Write("Неверный формат! Введите корректную дату: ");
                    date = DataProcessing.ParseDate(Console.ReadLine());
                }
            }

            return date;
        }

        static bool CheckId(int id)
        {
            var flag = true;
            foreach (var person in DataProcessing.PeopleList)
            {
                if (person.Id == id) 
                {
                    flag = false;
                    break;
                }
            }
            
            return flag;
        }
    }
}