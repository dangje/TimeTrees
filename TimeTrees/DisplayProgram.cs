﻿using System;

namespace TimeTrees
{
    public class DisplayProgram
    {
        public static void Run()
        {
            var flag = true;
            do
            {
                Console.WriteLine("1 Вывести список людей \n" +
                                  "2 Вывести список событий");
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.D1)
                {
                    var selectedPerson = ChoosePerson();
                    Console.WriteLine($"Имя: {selectedPerson.Name} \n" +
                                      $"Дата рождения: {selectedPerson.Born}");
                    if (selectedPerson.Death is null)
                        Console.WriteLine("Жив");
                    else Console.WriteLine($"Дата смерти: {selectedPerson.Death}");
                    if (selectedPerson.Parents.Length == 0)
                        Console.WriteLine("Нет информации о родителях");
                    else
                    {
                        Console.Write("Родители: ");
                        foreach (var personParent in selectedPerson.Parents)
                            Console.Write($"{personParent.Name}, ");
                    }

                    flag = false;
                }

                else if (keyInfo.Key == ConsoleKey.D2)
                {
                    var selectedEvent = ChooseTimeEvent();
                    Console.WriteLine($"Дата: {selectedEvent.Date} \n" +
                                      $"Описание события: {selectedEvent.Event}");
                    if(selectedEvent.Peoples.Count == 0)
                        Console.WriteLine("Нет информации о участниках");
                    else
                    {
                        Console.Write("Участники: ");
                        foreach (var people in selectedEvent.Peoples)
                        {
                            Console.Write($"{people.Name}, ");
                        }
                    }

                    flag = false;
                }
                else Console.WriteLine("Не верный ввод");
                
            } while (flag);
        }

        static People ChoosePerson()
        {
            var selectedNum = 0;
            do
            {
                Console.Clear();
                PrintPeople(selectedNum);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    selectedNum = selectedNum < DataProcessing.PeopleList.Count - 1
                        ? ++selectedNum
                        : 0;
                }
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    selectedNum = selectedNum != 0
                        ? --selectedNum
                        : DataProcessing.PeopleList.Count - 1;
                }
                else if (keyInfo.Key == ConsoleKey.Enter)
                {
                    Console.Clear();
                    return DataProcessing.PeopleList[selectedNum];
                }
            } while (true);
        }

        static Timeline ChooseTimeEvent()
        {
            var selectedNum = 0;
            do
            {
                Console.Clear();
                PrintTimeEvent(selectedNum);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    selectedNum = selectedNum < DataProcessing.TimeLineList.Count - 1
                        ? ++selectedNum
                        : 0;
                }
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    selectedNum = selectedNum != 0
                        ? --selectedNum
                        : DataProcessing.TimeLineList.Count - 1;
                }
                else if (keyInfo.Key == ConsoleKey.Enter)
                {
                    Console.Clear();
                    return DataProcessing.TimeLineList[selectedNum];
                }
            } while (true);
        }
        static void PrintPeople(int selectedNum)
        {
            for (var i = 0; i < DataProcessing.PeopleList.Count; i++)
            {
                if (selectedNum == i)
                    Console.BackgroundColor = ConsoleColor.White;
                Console.WriteLine(DataProcessing.PeopleList[i].Name);
                Console.BackgroundColor = ConsoleColor.Black;
            }
        }

        static void PrintTimeEvent(int selectedNum)
        {
            for (var i = 0; i < DataProcessing.TimeLineList.Count; i++)
            {
                if (selectedNum == i)
                    Console.BackgroundColor = ConsoleColor.White;
                Console.WriteLine(DataProcessing.TimeLineList[i].Event);
                Console.BackgroundColor = ConsoleColor.Black;
            }
        }
    }
}