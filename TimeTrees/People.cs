﻿using System;

namespace TimeTrees
{
    public class People
    {
        public int Id;
        public string Name;
        public DateTime Born;
        public DateTime? Death;
        public People[] Parents = new People[2];
    }
}
